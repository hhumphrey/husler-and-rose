<?php
/**
 * Roots initial setup and constants
 */
function roots_setup() {
  // Make theme available for translation
  load_theme_textdomain('roots', get_template_directory() . '/lang');

  // Register wp_nav_menu() menus (http://codex.wordpress.org/Function_Reference/register_nav_menus)
  register_nav_menus(array(
    'primary_navigation' => __('Primary Navigation', 'roots'),
  ));

  // Add post thumbnails (http://codex.wordpress.org/Post_Thumbnails)
  add_theme_support('post-thumbnails');
  // set_post_thumbnail_size(150, 150, false);
  // add_image_size('category-thumb', 300, 9999); // 300px wide (and unlimited height)
  add_image_size('gallery-large', 740, 505, true);
  add_image_size('gallery-thumb', 68, 48, true);
  add_image_size('archive-thumb', 236, 168, true);

  add_image_size('feature-portrait', 320, 480, true);
  add_image_size('feature-landscape', 320, 215, true);

  // retina

  add_image_size('gallery-large_x2', 1480, 1100, true);
  add_image_size('gallery-thumb_x2', 136, 96, true);
  add_image_size('archive-thumb_x2', 472, 336, true);

  add_image_size('feature-portrait_x2', 640, 960, true);
  add_image_size('feature-landscape_x2', 640, 430, true);

  // Add post formats (http://codex.wordpress.org/Post_Formats)
  // add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));

  // Tell the TinyMCE editor to use a custom stylesheet
  add_editor_style('/assets/css/editor-style.css');
}
add_action('after_setup_theme', 'roots_setup');

// Backwards compatibility for older than PHP 5.3.0
if (!defined('__DIR__')) { define('__DIR__', dirname(__FILE__)); }
