<?php
/**
 * Custom functions
 */

function klCreateAssignmentDropdown( $post_type, $title = 'Assign', $meta_query = null )
{
    $args = array( 
        'numberposts' => -1, 
        'post_type'=> $post_type, 
        'order' => 'ASC', 
        'orderby' => 'title', 
        'meta_query' =>  $meta_query );

    $posts = get_posts( $args );

    $arrposts;
    $list[ $post_type ] = "=== ".$title." ===";
    foreach( $posts as $post):
        $list[ $post->ID ] = $post->post_title;
    endforeach;

    return $list;
 }

function woocommerceHeaderAddToCartFragment( $fragments ) {
    global $woocommerce;
    ob_start();
    
        get_template_part('templates/shop-cart');

    $fragments['a.cart-contents'] = ob_get_clean();

    return $fragments;
}

add_filter('add_to_cart_fragments', 'woocommerceHeaderAddToCartFragment');

if(!function_exists('get_post_top_ancestor_id')){
/**
 * Gets the id of the topmost ancestor of the current page. Returns the current
 * page's id if there is no parent.
 * 
 * @uses object $post
 * @return int 
 */
function get_post_top_ancestor_id(){
    global $post;
    
    if($post->post_parent){
        $ancestors = array_reverse(get_post_ancestors($post->ID));
        return $ancestors[0];
    }
    
    return $post->ID;
}}

function fetchInstagramData( ){
    $transient = "instagram";
    $expiration = 1800; // 30 mins
    $url = "https://api.instagram.com/v1/users/514424678/media/recent/?access_token=514424678.a476062.3437af49c99a417c8558e887399e3e1b&count=1";
    // https://instagram.com/oauth/authorize/?client_id=CLIENT-ID&redirect_uri=REDIRECT-URI&response_type=token

    if ( false === ( $result = get_transient( $transient ) ) ){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $result = curl_exec($ch);
        curl_close($ch);

        set_transient( $transient, $result, $expiration );
    }
    
    return json_decode($result);
}
 