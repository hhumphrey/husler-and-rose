<?php

/**
 * Registers a post type with default values which can be overridden as needed.
 * @param string $singular title of the post type
 * @param string $plural title of the post type
 * @param array $args overrides the defaults
 * @param array $supports can be 'title', 'thumbnail', 'editor' and 'comments'
 */
function klRegisterPostType( $singular, $plural, $args = array(), $supports ){
    $sanitizedTitle = sanitize_title( $singular );
 
    $defaults = array(
                'labels' => array(
                'name' => $plural,
                'singular_name' => $singular,
                'add_new_item' => 'Add New ' . $singular,
                'edit_item' => 'Edit ' . $singular,
                'new_item' => 'New ' . $singular,
                'search_items' => 'Search ' . $plural,
                'not_found' => 'No ' . $plural . ' found',
                'not_found_in_trash' => 'No ' . $plural . ' found in trash'
            ),
            '_builtin' => false,
            'public' => true,
            'hierarchical' => false,
            'taxonomies' => array( ),
            'query_var' => true,
            'menu_position' => 6,
            'supports' => $supports,
            'rewrite' => array( 'slug' => $sanitizedTitle ),
            'has_archive' => true
    );
 
    $args = wp_parse_args( $args, $defaults );
    $postType = isset( $args['postType'] ) ? $args['postType'] : $sanitizedTitle;
    register_post_type( $postType, $args );
}

/**
 * Register all the sites post types using klRegisterPostType
 */
function klSetupPostTypes() {
    // klRegisterPostType( 'Example', 'Examples', array( 'postType' => 'example', rewrite => array( 'slug' => 'examples' ) ), array( 'title', 'thumbnail', 'editor', 'comments' ) );
}

add_action( 'init', 'klSetupPostTypes');
