<?

function klCreateTaxonomyLabels( $singular, $plural )
{
    $arrTaxonomy = array(
        'name' => _x(  $plural, 'taxonomy general name' ),
        'singular_name' => _x(  $singular, 'taxonomy singular name' ),
        'search_items' =>  __( 'Search '.$plural ),
        'popular_items' => __( 'Popular '.$plural ),
        'all_items' => __( 'All '.$plural ),
        'parent_item' => __(  $singular ),
        'parent_item_colon' => __(  $singular.':' ),
        'edit_item' => __( 'Edit '. $singular ),
        'update_item' => __( 'Update '. $singular ),
        'add_new_item' => __( 'Add '. $singular.' type' ),
        'new_item_name' => __( 'New '. $singular.' name' ),
        'separate_items_with_commas' => __( 'Separate '.strtolower( $plural ).' with commas' ),
        'add_or_remove_items' => __( 'Add or remove '.strtolower( $plural ) ),
        'choose_from_most_used' => __( 'Choose from the most used '.strtolower( $plural ) ),
        'menu_name' => __( $plural ),
    );

    return $arrTaxonomy;
}

function klRegisterTaxonomies()
{
    foreach ( glob( plugin_dir_path( __FILE__ ).'taxonomies/*.php' ) as $file )
    {
        require_once $file;
    }
}

add_action( 'init', 'klRegisterTaxonomies' );
