//The build will inline common dependencies into this file.
requirejs.config({
  baseUrl: '/assets/js',
  paths: {
    'jquery':                   'lib/jquery',
    'jquery-mobiletouch':       'plugins/jquery.mobile.touch',
    'jquery-preload':           'plugins/jquery.preload'
  },
  shim: {
    'query-mobiletouch':    [ 'jquery' ]
  }
});