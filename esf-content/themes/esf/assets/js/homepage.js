'use strict';

define([ 'mylibs/project-archive' ], function ( ProjectArchive ) {
    ProjectArchive.init( $( "#js-projectArchive" ) );
});