<?php while (have_posts()) : the_post();
?>
<div class="col-lg-3">
	<div class="row">
		<? the_content(); ?>
	</div>
	<div class="row">
		<form action="/search" class="signup" method="post">
                    <fieldset>
                        <label for="signup">
                            Newsletter updates
                        </label>
                        <div class="search-inputs">
                            <div>
                                <input type="text" name="signup" id="js-newslettersignup" placeholder="Email Address">
                                <input id="js-newslettersubmit" type="submit" value="Submit">
                            </div>
                        </div>
                    </fieldset>
                    <div id="js-newsletterwarnings">
                        
                        
                    </div>
                </form>
	</div>
</div>
<?
endwhile; ?>


<div class="col-lg-9">
	
		<? 
		if(get_field('homepage_scroller_content')): 
			$item_count = count( get_field('homepage_scroller_content') ); ?>

			<div class="row">
				<div class="col-lg-12">
					<div id="js-homepage-carousel" class="carousel slide">

						<ol class="carousel-indicators">
							<? for( $i=0; $i<$item_count; $i++ ): ?>
						    	<li data-target="#js-homepage-carousel" data-slide-to="<?=$i;?>"></li>
						    <? endfor; ?>
						 </ol>

						<div class="carousel-inner">
						<? 
						while(has_sub_field('homepage_scroller_content')): 
							$post_object = get_sub_field('homepage_assigned_content');
							$post_desc = get_sub_field('homepage_assigned_content_description');

							$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_object->ID), 'gallery-large' );
						?>
							<div class="item active">
								<img src="<?=$featured_image[0]?>" />
								<div class="carousel-caption"><?=$post_desc?></div>
							</div>

						<? 
						endwhile; ?>
						</div>
					</div>
				</div>
			</div>

		<? 
		endif; ?>

		<div class="row">
			<h3 class="col-lg-12">Latest Products</h3>
			<?
			if(get_field('homepage_latest_products')): 
				while(has_sub_field('homepage_latest_products')):
					$post_object = get_sub_field('homepage_assign_products'); 
					$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_object->ID), 'archive-thumb' ); ?>
					<div class="col-lg-4">
						<a href="<?=get_permalink($post_object->ID);?>">
							<img src="<?=$featured_image[0]?>" />
							<p><?=$post_object->post_title; ?></p>
						</a>
					</div>
			<?
				endwhile; 
			endif; ?>

		</div>
		<div class="row">
			<?
			if($post_object = get_field('homepage_blog_post')): 
				$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id($post_object->ID), 'archive-thumb' ); ?>
				<div class="col-lg-4">
					<h3>Blog</h3>
					<a href="<?=get_permalink($post_object->ID);?>">
							<img src="<?=$featured_image[0]?>" />
							<p><?=$post_object->post_title; ?></p>
						</a>
				</div>
			<?
			endif; ?>

			<div class="col-lg-4">
				<h3>Instagram</h3>
				<?
				$instagram_data = fetchInstagramData();
				foreach ($instagram_data->data as $post): ?>
					<a href="<?=$post->link;?>">
						<img src="<?=$post->images->low_resolution->url;?>" />
						<p>
							<?=$post->likes->count;?> likes<br />
							<?=$post->comments->count;?> comments
						</p>
					</a>
	    		<?
	    		endforeach; ?>
			</div>

			<div class="col-lg-4">
				<h3>Twitter</h3>
			</div>
		</div>
	</div>
	
</div>

<script>
	//$('.carousel').carousel()
</script>