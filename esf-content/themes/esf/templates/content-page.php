<?php while (have_posts()) : the_post(); 
	$all_parents = array_reverse( get_post_ancestors( $post->ID ) );
	$wp_query = new WP_Query();

	$children_args = array( 
	    'post_parent' => $post->ID,
	    'post_type'   => 'page', 
	    'numberposts' => -1,
	    'post_status' => 'publish',
	    'orderby'	  => 'menu_order'
	);

	$children = get_posts( $children_args );

	$featured_image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'gallery-large' );

	endwhile; 

	$args = array(
		'posts_per_page'   => -1,
		'post_parent'	   =>  $all_parents[0],
		'sort_column' 	   => 'menu_order',
		'order'            => 'ASC',
		'post_type'    => 'page',
	    'post_status'  => 'publish' 
	);

	$pages = get_posts( $args );
?>

<div class="col-lg-3">
	<ul class="nav">
		<? 
		foreach( $pages as $page ): ?>
			<li>
				<a href="<?=get_permalink( $page->ID );?>"><?=$page->post_title;?></a>
				<? 
				if( $page->ID == $post->ID ): ?>
				<ul>
				<?
					foreach( $children as $child ): ?>
						<li><a href="#<?=$child->post_name;?>"><?=$child->post_title;?></a></li>
					<?
					endforeach; ?>
				</ul>
				<?
				endif; ?>
			</li>
		<?
		endforeach; ?>
	</ul>
</div>
<?
?>

<div class="col-lg-9">
	<img src="<?=$featured_image[0]?>" />
	<div class="row">
	<div class="col-lg-9">
<?
/*
<?php if(get_field('repeater_field_name')): ?>
 
	<ul>
 
	<?php while(has_sub_field('repeater_field_name')): ?>
 
		<li>sub_field_1 = <?php the_sub_field('sub_field_1'); ?>, sub_field_2 = <?php the_sub_field('sub_field_2'); ?>, etc</li>
 
	<?php endwhile; ?>
 
	</ul>
 
<?php endif; ?>
*/
foreach( $children as $post ): setup_postdata( $post );
?>

<div id="<?=$post->post_name; ?>">
	<h2><? the_title(); ?></h2>
	<? the_content(); ?>
	<? if( get_field( 'image_gallery' ) ):
		
	?>
    
    	<div class="row">
            <? while( has_sub_field('image_gallery')):
                $image = get_sub_field('uploaded_image');

                if( $image['width'] > $image['height']  ){
					$img_size = 'feature-landscape';
				}else{
					$img_size = 'feature-portrait';
				}
            ?>
	            <div class="col-lg-6">
	                <img src="<?=$image['sizes'][$img_size]?>" class="img-responsive" />
	            </div>
            <? endwhile; ?>
        </div>
    <? endif; ?>
</div>

<?  
endforeach; ?>
		</div>
	</div>
</div>