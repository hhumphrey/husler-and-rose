<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

global $is_taxonomy;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header('shop'); ?>

<div class="col-lg-3">
	<div class="row">
		<ul class="product-cats">
		<?
		$args = array(
		    'style'              => 'list',
		    'show_count'         => 0,
		    'use_desc_for_title' => 1,
		    'child_of'           => 0,
		    'title_li'           => __( '' ),
		    'show_option_none'   => __('No Menu Items'),
		    'number'             => null,
		    'echo'               => 1,
		    'depth'              => 2,
		    'taxonomy'           => 'product_cat',
		    'hide_empty' 		 => 1,
		    'exclude'			 => 5
		);
			wp_list_categories( $args );
		?>
		</ul>
	</div>
</div>

<div class="col-lg-9">
	<div class="row">

	<?php
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action('woocommerce_before_main_content');
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="page-title"><?php woocommerce_page_title(); ?></h1>

		<?php endif; ?>

		<?php do_action( 'woocommerce_archive_description' ); ?>
		<? if( !$is_taxonomy ): ?>
		<h2>Our Pick</h2>
		<ul class="products">
		<?
			$args = array( 
				'posts_per_page'	=> -1,
				'product_cat'         	=> 'our-pick',
				'post_type'			=> 'product'
			);

			$our_pick = get_posts( $args );
			foreach( $our_pick as $post ): setup_postdata( $post );
		?>
			<?php woocommerce_get_template_part( 'content', 'product' ); ?>
		<? 
			endforeach;
			wp_reset_postdata(); ?>
		</ul>
	<? endif; ?>

		<h2>All Products</h2>
		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php woocommerce_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

		<?
		if( !$is_taxonomy ):
			$meta_query = array();
			$meta_query[] = array(
				'key'     => '_visibility',
				'value'   => array( 'visible', 'catalog' ),
				'compare' => 'IN'
			);
			$meta_query[] = array(
				'key' 		=> '_stock_status',
				'value' 	=> 'outofstock',
				'compare' 	=> '='
			);

			$args = array(
				'post_type'	=> 'product',
				'post_status' => 'publish',
				'ignore_sticky_posts'	=> 1,
				'posts_per_page' => $per_page,
				'orderby' => $orderby,
				'order' => $order,
				'meta_query' => $meta_query
			);

			$sold_out = get_posts( $args ); 

			if( count( $sold_out ) > 0  ): ?>
			<h2>Sold Products</h2>
			<ul class="products">
				<? 
				foreach( $our_pick as $post ): setup_postdata( $post );
				?>
					<?php woocommerce_get_template_part( 'content', 'product' ); ?>
				<? 
					endforeach;
					wp_reset_postdata(); ?>
			</ul>
		<? endif; 
		endif;
		?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
	?>
</div>
</div>

<?php get_footer('shop'); ?>